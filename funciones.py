import pygame, sys, random
#creacion de la matriz
#16 columnas * 9 filas
mapa1 = [
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1],
        [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
        [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
]
#Inicialitzar
pygame.init()
#MEDIDAS
ANCHO = 1280
ALTO= 705
#COLORES
BLANCO = (255,255,255)
NEGRO = (0,0,0)
NARANJA = (255,128,0)
VERDE = (0,255,0)
#VENTANA
ventana = pygame.display.set_mode((ANCHO, ALTO))
reloj = pygame.time.Clock()
fuente = pygame.font.SysFont("arial black", 20)
#imagen
pared = pygame.image.load("./imagenes/wall.png").convert_alpha()
suelo = pygame.image.load("./imagenes/suelo.png").convert_alpha()
personaje = pygame.image.load("./imagenes/personaje.png").convert_alpha()
malo = pygame.image.load("./imagenes/malote.png").convert_alpha()
malo.set_colorkey(BLANCO)
personaje.set_colorkey(BLANCO)
#construir mapa
jugando = True
#coordenadas personaje
x_personaje = 1
y_personaje = 1
x_malo = 8
y_malo = 2
#funciones
#----creacion del 
direccion = ''
while jugando:
    reloj.tick(60)
    movimiento = False
    #imprimir mapa
    for fila in range(len(mapa1)):
        for baldosa in range(len(mapa1[0])):
            paredrect = pared.get_rect().move(baldosa*64, fila*64)
            suelorect = suelo.get_rect().move(baldosa*64, fila*64)
            if mapa1[fila][baldosa] == 1:
                ventana.blit(pared,paredrect)
            else:
                ventana.blit(suelo,paredrect)
    personajerect = pared.get_rect().move(x_personaje*64, y_personaje*64)
    ventana.blit(personaje,personajerect)
    malorect= malo.get_rect().move(x_malo*64,y_malo*64)
    ventana.blit(malo,malorect)
    #eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugando = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                jugando = False
            if event.key == pygame.K_RIGHT:
                direccion = "derecha"
            if event.key == pygame.K_LEFT:
                direccion = "izquierda"
            if event.key == pygame.K_DOWN:
                direccion = "abajo"
            if event.key == pygame.K_UP:
                direccion = "arriba"
        '''if event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                direccion = 'parar'
            if event.key == pygame.K_LEFT:
                direccion = 'parar'
            if event.key == pygame.K_DOWN:
                direccion = 'parar'
            if event.key == pygame.K_UP:
                direccion = 'parar'
        '''
    #Logica
    if direccion == "derecha":
        new_y = y_personaje
        new_x = x_personaje + 1
        movimiento = True
        if mapa1[new_y][new_x] == 0:
            x_personaje += 1
    if direccion == "izquierda":
        new_y = y_personaje
        new_x = x_personaje -1
        movimiento = True
        if mapa1[new_y][new_x] == 0:
            x_personaje -= 1  
    if direccion == "arriba":
        new_y = y_personaje -1
        new_x = x_personaje
        movimiento = True
        if mapa1[new_y][new_x] == 0:
            y_personaje -= 1
    if direccion == "abajo":
        new_y = y_personaje + 1
        new_x = x_personaje
        movimiento = True
        if mapa1[new_y][new_x] == 0:
            y_personaje += 1
    direccion = ""
    #Logica del malote
    if movimiento == True:
        new_x = x_malo + random.randint(-1,1)
        new_y = y_malo + random.randint(-1,1)
        if mapa1[new_y][new_x] == 0:
            y_malo = new_y 
            x_malo = new_x
    #TANATHOS
    if y_malo == y_personaje and x_malo == x_personaje:
        jugando = False
    
    pygame.display.flip()
pygame.quit()
