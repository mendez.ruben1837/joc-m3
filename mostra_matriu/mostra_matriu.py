import sys, pygame

background=pygame.image.load("crystal_wall02.png")

pygame.init()
n_rows=int(480/32)
n_cols=int(640/32)
screen = pygame.display.set_mode([640, 480])
going=True
while going:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False       
    for row in range(0, n_rows):
        for col in range(0, n_cols):
            square=background.get_rect().move(col*32, row*32)
            screen.blit(background, square)
    pygame.display.flip()
pygame.quit()
