import pygame
    
imatge=0 # l'índex de la imatge a mostrar a ch_esq o ch_dreta
orientacio=0 # cap on mira el personatge 0=esquerra, 1=dreta
ch_esq=[
    pygame.image.load("./frame-1.png"),
    pygame.image.load("./frame-2.png")]
ch_dreta=[ # transform.flip volteja una imatge
    pygame.transform.flip(ch_esq[0], True, False),
    pygame.transform.flip(ch_esq[1], True, False)
]
x=400 # posició on es troba el personatge

pygame.init()
screen = pygame.display.set_mode([1280, 800])

going=True
while going:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            going=False       
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                if x<700:
                    x+=10
                    orientacio=1
            if event.key == pygame.K_LEFT:
                if x>40:
                    x-=10
                    orientacio=0
    if orientacio==1:
        square=ch_dreta[imatge].get_rect().move(x, 0)
        screen.blit(ch_dreta[imatge], square)
    else:
        square=ch_esq[imatge].get_rect().move(x, 0)
        screen.blit(ch_esq[imatge], square)
    
    pygame.display.flip()
    imatge=(imatge+1)%2 # si la imatge és 0 passa a 1, si és 1 passa a 0
pygame.quit()
