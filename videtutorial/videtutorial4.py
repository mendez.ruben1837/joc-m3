import pygame
#desplegar juego
pygame.init()
#crear la ventana
ancho = 1280
alto = 720
#direccion nave
direccion = "arriba"
#FUNCIONES
def nave_arriba(ventana, pos_x, pos_y):
    pygame.draw.rect(ventana, AZUL, (pos_x, pos_y, 60, 60))
    pygame.draw.rect(ventana, ROJO, (pos_x, pos_y, 15, 30))
    pygame.draw.rect(ventana, ROJO, (pos_x + 45, pos_y, 15, 30))
def nave_abajo(ventana, pos_x, pos_y):
    pygame.draw.rect(ventana, AZUL, (pos_x, pos_y, 60, 60))
    pygame.draw.rect(ventana, ROJO, (pos_x, pos_y + 30, 15, 30))
    pygame.draw.rect(ventana, ROJO, (pos_x + 45, pos_y + 30, 15, 30))
def nave_derecha(ventana, pos_x, pos_y):
    pygame.draw.rect(ventana, AZUL, (pos_x, pos_y, 60, 60))
    pygame.draw.rect(ventana, ROJO, (pos_x+30, pos_y, 30, 15))
    pygame.draw.rect(ventana, ROJO, (pos_x+30, pos_y + 45, 30, 15))
def nave_izquierda(ventana, pos_x, pos_y):
    pygame.draw.rect(ventana, AZUL, (pos_x, pos_y, 60, 60))
    pygame.draw.rect(ventana, ROJO, (pos_x, pos_y, 30, 15))
    pygame.draw.rect(ventana, ROJO, (pos_x, pos_y + 45, 30, 15))
def asteroide_1(ventana, x, y):
    pygame.draw.rect(ventana, VERDE,(x,y, 60, 60))
    pygame.draw.rect(ventana, ROJO,(x,y, 20, 20))
def asteroide_2(ventana, x, y):
    pygame.draw.rect(ventana, VERDE,(x,y, 60, 60))
    pygame.draw.rect(ventana, ROJO,(x + 40,y, 20, 20))
def asteroide_3(ventana, x, y):
    pygame.draw.rect(ventana, VERDE,(x,y, 60, 60))
    pygame.draw.rect(ventana, ROJO,(x + 40,y + 40, 20, 20))
def asteroide_4(ventana, x, y):
    pygame.draw.rect(ventana, VERDE,(x,y, 60, 60))
    pygame.draw.rect(ventana, ROJO,(x,y + 40, 20, 20))
#datos
contador = 0
x = 100
velocidad_x = 0.5
y = 100
pos_x = 1000
pos_x_velocidad = 0
pos_y_velocidad = 0
pos_y = 400
#COLORES
BLANCO = (255,255,255)
NEGRO = (0,0,0)
ROJO = (255,0,0)
VERDE = (0,255,0)
AZUL = (0,0,255)
#VENTANA
ventana = pygame.display.set_mode((ancho,alto))
fuente = pygame.font.SysFont("aakar bold", 200)
#BUCLE PRINCIPAL
jugando = True
while jugando:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugando = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                jugando = False
            if event.key == pygame.K_RIGHT:
                direccion = "derecha"
                pos_x_velocidad = 0.1
            if event.key == pygame.K_LEFT:
                direccion = "izquierda"
                pos_x_velocidad = -0.1
            if event.key == pygame.K_UP:
                direccion = "arriba"
                pos_y_velocidad = -0.1
            if event.key == pygame.K_DOWN:
                direccion = "abajo"
                pos_y_velocidad = 0.1
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                pos_x_velocidad = 0
            if event.key == pygame.K_LEFT:
                pos_x_velocidad = 0
            if event.key == pygame.K_UP:
                pos_y_velocidad = 0
            if event.key == pygame.K_DOWN:
                pos_y_velocidad = 0
    #logica de movimiento
    x += velocidad_x
    if x > ancho + 50:
        x = -50
    pos_x += pos_x_velocidad
    pos_y += pos_y_velocidad
    if pos_x > ancho + 50:
        pos_x = -50
    if pos_y > alto + 50:
        pos_y = -50
    # if pos_x > -ancho:
    #     pos_x = 50
    # if pos_y > -alto:
    #     pos_y = 50
    #rellenar la pantalla de un color
    ventana.fill(ROJO)

    #dibujos
    contador += 0.02
    if contador >= 41:
        contador = 1
    if contador < 11:
        asteroide_1(ventana, x ,y)
    elif contador < 21:
        asteroide_2(ventana, x ,y)
    elif contador < 31:
        asteroide_3(ventana, x ,y)
    elif contador < 41:
        asteroide_4(ventana, x ,y)
    if direccion == "arriba":
        nave_arriba(ventana, pos_x, pos_y)
    elif direccion == "derecha":
        nave_derecha(ventana, pos_x, pos_y)
    elif direccion == "izquierda":
        nave_izquierda(ventana, pos_x, pos_y)
    elif direccion == "abajo":
        nave_abajo(ventana, pos_x, pos_y)   
    #actualizar constantemente
    texto_puntos = fuente.render("Hola Lilou <3", True, BLANCO)
    ventana.blit(texto_puntos, (400,200))
    pygame.display.update()
pygame.quit()
