import pygame
import random
#Inicialitzar
pygame.init()
#MEDIDAS
ANCHO = 1280
ALTO= 720
#COLORES
BLANCO = (255,255,255)
NEGRO = (0,0,0)
NARANJA = (255,128,0)
VERDE = (0,255,0)
#funciones
def colision(x1,y1,a1,b1,x2,y2,a2,b2,ex=0):
    if x1 + a1 > x2 + ex and x1 + ex < x2 + a2 and y1 + b1 > y2 + ex and y1 + ex < y2 + b2:
        return True
    else:
        return False

#VENTANA
ventana = pygame.display.set_mode((ANCHO, ALTO))
reloj = pygame.time.Clock()
fuente = pygame.font.SysFont("arial black", 20)
#Imagenes
fondo = pygame.image.load("fondo.png").convert_alpha()
nave_arriba = pygame.image.load("Nave_arriba.png").convert_alpha()
nave_abajo = pygame.image.load("Nave_abajo.png").convert_alpha()
nave_izquierda = pygame.image.load("Nave_izquierda.png").convert_alpha()
nave_derecha = pygame.image.load("Nave_derecha.png").convert_alpha()

nave_arriba.set_colorkey(BLANCO)
nave_abajo.set_colorkey(BLANCO)
nave_izquierda.set_colorkey(BLANCO)
nave_derecha.set_colorkey(BLANCO)

asteroide_1 = pygame.image.load("asteroide_1.png").convert_alpha()
asteroide_2 = pygame.image.load("asteroide_2.png").convert_alpha()
asteroide_3 = pygame.image.load("asteroide_3.png").convert_alpha()
asteroide_4 = pygame.image.load("asteroide_4.png").convert_alpha()

asteroide_1.set_colorkey(BLANCO)
asteroide_2.set_colorkey(BLANCO)
asteroide_3.set_colorkey(BLANCO)
asteroide_4.set_colorkey(BLANCO)

aste_1 = [asteroide_1, asteroide_2, asteroide_3, asteroide_4]
aste_3 = [asteroide_2, asteroide_3, asteroide_4, asteroide_1]
aste_4 = [asteroide_3, asteroide_4, asteroide_1, asteroide_2]
aste_5 = [asteroide_4, asteroide_1, asteroide_2, asteroide_3]

asteroides_grandes = []
for i in range(10):
    x = random.randint(0, ANCHO)
    y = random.randint(50, ALTO-120)
    v = random.randint(1,3)
    f = random.choice([aste_1, aste_3, aste_4, aste_5])
    a = [f,x,y,v]
    asteroides_grandes.append(a)
#Datos
vidas = 3
nivel = 1
nave_pos_x = 600
nave_pos_y = 670
nave_vel_x = 0
nave_vel_y = 0
direccion = "arriba"

frames_asteroides = 0 

jugando = True
while jugando:
    reloj.tick(60)
    #Eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugando = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                jugando = False
            if event.key == pygame.K_RIGHT:
                direccion = "derecha"
                nave_vel_x = 2
            if event.key == pygame.K_LEFT:
                direccion = "izquierda"
                nave_vel_x = -2
            if event.key == pygame.K_DOWN:
                direccion = "abajo"
                nave_vel_y = 2
            if event.key == pygame.K_UP:
                direccion = "arriba"
                nave_vel_y = -2
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_ESCAPE:
                jugando = False
            if event.key == pygame.K_RIGHT:
                nave_vel_x = 0
            if event.key == pygame.K_LEFT:
                nave_vel_x = 0
            if event.key == pygame.K_DOWN:
                nave_vel_y = 0
            if event.key == pygame.K_UP:
                nave_vel_y = 0
    #Logica
    for a in asteroides_grandes:
        a[1] += a[3]
        if a[1] > ANCHO:
            a[1] = -64
    nave_pos_x += nave_vel_x
    nave_pos_y += nave_vel_y
    if nave_pos_x > ANCHO -32:
        nave_pos_x = ANCHO -32
    if nave_pos_x < 0:
        nave_pos_x = 0
    if nave_pos_y > ALTO - 32:
        nave_pos_y = ALTO - 32

    if nave_pos_y < 10:
      jugando = False
    
    for a in asteroides_grandes:
        if colision(a[1], a[2], 64, 64, nave_pos_x, nave_pos_y, 32, 32, 15):
            jugando = False
    #Imagenes
    ventana.blit(fondo, (0, 0))

    texto1 = fuente.render("Nivel: " + str(nivel), True, BLANCO)
    texto2 = fuente.render("Vidas: " + str(vidas), True, BLANCO)

    ventana.blit(texto1, (20,10))
    ventana.blit(texto2, (1150,10))

    frames_asteroides += 1
    if frames_asteroides >= 41:
        frames_asteroides = 1
    if frames_asteroides < 11:
        for a in asteroides_grandes:
            ventana.blit(a[0][1], (a[1], a[2]))
    elif frames_asteroides < 21:
        for a in asteroides_grandes:
            ventana.blit(a[0][1], (a[1], a[2]))
    elif frames_asteroides < 31:
        for a in asteroides_grandes:
            ventana.blit(a[0][2], (a[1], a[2]))
    elif frames_asteroides < 41:
        for a in asteroides_grandes:
            ventana.blit(a[0][3], (a[1], a[2]))

    if direccion == "arriba":
        ventana.blit(nave_arriba, (nave_pos_x, nave_pos_y))
    elif direccion == "abajo":
        ventana.blit(nave_abajo, (nave_pos_x, nave_pos_y))
    elif direccion == "derecha":
        ventana.blit(nave_derecha, (nave_pos_x, nave_pos_y))
    elif direccion == "izquierda":
        ventana.blit(nave_izquierda, (nave_pos_x, nave_pos_y))
    pygame.display.update()
pygame.quit()
