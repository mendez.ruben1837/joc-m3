import pygame
#desplegar juego
pygame.init()
#crear la ventana
ancho = 1280
alto = 720
#datos
x = 100
y = 100
pos_x = 100
pos_y = 100
#COLORES
BLANCO = (255,255,255)
NEGRO = (0,0,0)
ROJO = (255,0,0)
VERDE = (0,255,0)
AZUL = (0,0,255)
puntos = 0
vueltas = 0
#VENTANA
ventana = pygame.display.set_mode((ancho,alto))
fuente = pygame.font.SysFont("aakar bold", 200)
print(ventana)
print(fuente)
#BUCLE PRINCIPAL
jugando = True
while jugando:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            jugando = False
        if event.type == pygame.KEYDOWN:
            if event.type == pygame.K_a:
                vueltas += 1
    #logica de movimiento
    x += 1
    if x > ancho+50:
        x = -100
    pos_x +=1
    pos_y +=1
    if pos_x > ancho:
        pos_x = -50
        puntos +=1
    if pos_y > alto:
        pos_y = -50
    #rellenar la pantalla de un color
    ventana.fill(ROJO)
    #puntuacion
    texto_puntos = fuente.render("puntos: " + str(puntos), True, BLANCO)
    texto_vueltas = fuente.render("vueltas: " + str(vueltas), True, BLANCO)
    #dibujar sobre ella un rectangulo
    pygame.draw.circle(ventana, VERDE, (x,y),50)
    ventana.blit(texto_puntos, (600,200))
    ventana.blit(texto_vueltas, (500, 400))
    pygame.draw.rect(ventana, AZUL, (pos_x, pos_y, 50, 50))
    #actualizar constantemente
    pygame.display.update()
pygame.quit()
